# Cours NSI Terminale

## Découpage

<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;margin:auto;" >
<body>
<tr>
<td style="text-align: center;"><a title="Modularité" href="Modularité/"><img src='assets/modular.svg' width="128px"/><br/>Modularité</a></td>
<td style="border: none;text-align: center;"><a title="Récursivité" href="Recursivité/"><img src='assets/recursivite.svg' width="128px"/><br/>Récursivité</a></td>
<td style="border: none;text-align: center;"><a title="Programmation Orientée Objet" href="POO"><img src='assets/poo.svg' width="128px"/><br/>Programmation Orientée Objet</a></td>
<td style="border: none;text-align: center;"><a title="Structures de données : Pile et File" href="Pile_et_File/"><img src='assets/pile_file.svg' width="128px"/><br/>Structures Pile et File</a></td>
<td style="border: none;text-align: center;"><a title="Arbres" href="Arbres/"><img src='assets/arbre.svg' width="128px"/><br/>Arbres et Arbres binaires</a></td>
<td style="border: none;text-align: center;"><a title="Graphes" href="Graphes/"><img src='assets/graphe.svg' width="128px"/><br/>Graphes</a></td>
</tr>
<tr>
<td style="text-align: center;"><a title="Bases de données" href="BDD_SGBD"><img src='assets/bdd.svg' width="128px"/><br/>Bases de données</a></td>
<td style="border: none;text-align: center;"><a title="SGBD" href="SQL"><img src='assets/sgbd.svg' width="128px"/><br/>SGBD</a></td>
<td style="border: none;text-align: center;"><a title="Routage et Algorithmes de routage" href="Routage"><img src='assets/routage.svg' width="128px"/><br/>Routage et Algorithmes de routage</a></td>
<td style="border: none;text-align: center;"><a title="Calculabilité" href="Calculabilite"><img src='assets/calculabilite.svg' width="128px"/><br/>Calculabilité</a></td>
<td style="border: none;text-align: center;"><a title="Recherche textuelle" href="Recherche_Textuelle"><img src='assets/recherche.svg' width="128px"/><br/>Recherche textuelle</a></td>
</tr>
</body>
</table>



### Progression

| Séquence                                      | Contenu                                                      | Projet associé |
| --------------------------------------------- | ------------------------------------------------------------ | -------------- |
| 1 - a : Modularité                            | Mise au point de bibliothèques, mise au point de programmes, debug |                |
| 1 - b : Récursivité                           | Programmation recursive, diviser pour régner, tri fusion     |                |
| 2 - a : Programmation orientée objet          |                                                              |                |
| 2 - b : Structure Pile & File                 |                                                              |                |
| 3 - a : Arbres, arbres binaires               |                                                              |                |
| 3 - b : Graphes                               |                                                              |                |
| 4 - a : Bases de données                      |                                                              |                |
| 4 - b : Système de Gestion de Base de Données |                                                              |                |
| 5 -a : Routage et algorithmes de routage      |                                                              |                |
| 5 - b : Calculabilité                         |                                                              |                |
| 6 : Recherche textuelle                       |                                                              |                |



### Sitographie

- [Gitlab de David Landry](https://gitlab.com/david_landry/nsi)
- [Framagit de Christophe Mieszczak](https://framagit.org/tofmzk/informatique_git/-/tree/master/terminale_nsi)
- [Site de David Roche](https://pixees.fr/informatiquelycee/n_site/nsi_term.html)
- 