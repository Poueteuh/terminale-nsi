### Projet : Créer un "Livre dont vous êtes le héros" 

> Objectif : écrire une histoire où les choix du lecteur influencent le déroulement de l'intrigue.

---

## Objectifs du projet
1. Comprendre comment utiliser les **arbres** pour structurer une narration.
2. Appliquer les concepts de **POO** pour modéliser des chapitres et des choix.
3. Développer un programme qui raconte une histoire interactive.

---

## Étape 1 : L'arbre narratif 

Un **livre dont vous êtes le héros** peut être représenté par un **arbre** :
- Chaque **chapitre** est un **nœud**.
- Chaque **choix** est une **branche** menant à un autre nœud.
- Une **fin d'histoire** correspond à une **feuille** (un nœud sans enfants).

### Exemple d'arbre
Voici un exemple d'arbre narratif simple :

```
               Introduction
                 /      \
    Aller au nord        Aller au sud
        |                    |
  Explorer la cabane      Trouver un trésor (fin)
        |
       Fin
```

Dans cet arbre :
- Le chapitre d'introduction propose 2 choix.
- Chaque choix mène à un autre chapitre (ou à une fin).

---

## Étape 2 : Modélisation avec la POO 

On va utiliser des **classes** pour structurer notre code.

### Classe `Chapitre`
Un chapitre représente une étape de l'histoire. Voici ses caractéristiques :
- **Attributs** :
  - `texte` : le contenu du chapitre.
  - `choix` : un dictionnaire qui associe une option à un chapitre suivant.
- **Méthodes** :
  - `ajouter_choix(choix, chapitre_suivant)` : ajoute un choix possible.
  - `suivant(choix)` : retourne le chapitre correspondant au choix.

### Classe `Livre`
Le livre gère toute l'histoire. Voici ses caractéristiques :
- **Attributs** :
  - `chapitre_depart` : le premier chapitre de l'histoire.
- **Méthodes** :
  - `lancer()` : démarre l'histoire et permet au lecteur de faire ses choix.

---

## Étape 3 : Implémentation 

Voici un **squelette de code** à compléter pour commencer.

### Classe `Chapitre`
```python
class Chapitre:
    def __init__(self, texte):
        self.texte = texte
        self.choix = {}  # Dictionnaire pour les choix possibles

    def ajouter_choix(self, choix, chapitre_suivant):
        self.choix[choix] = chapitre_suivant

    def suivant(self, choix):
        return self.choix.get(choix, None)
```

### Classe `Livre`
```python
class Livre:
    def __init__(self, chapitre_depart):
        self.chapitre_depart = chapitre_depart

    def lancer(self):
        chapitre_actuel = self.chapitre_depart
        while chapitre_actuel:
            print(chapitre_actuel.texte)
            if not chapitre_actuel.choix:
                print("Fin de l'histoire.")
                break
            print("\nChoix disponibles :")
            for i, choix in enumerate(chapitre_actuel.choix.keys(), 1):
                print(f"{i}. {choix}")
            choix_utilisateur = input("Votre choix : ")
            chapitre_actuel = chapitre_actuel.suivant(choix_utilisateur)
```

---

## Étape 4 : Construire une petite histoire 

Essayons de créer une histoire simple avec ces classes.

### Exemple
```python
# Création des chapitres
intro = Chapitre("Vous êtes dans une forêt sombre. Que voulez-vous faire ?")
aller_vers_nord = Chapitre("Vous trouvez une cabane abandonnée.")
aller_vers_sud = Chapitre("Vous tombez dans un trou et trouvez un trésor. Fin !")
explorer_cabane = Chapitre("Vous entrez dans la cabane. Fin !")

# Ajout des choix
intro.ajouter_choix("Aller vers le nord", aller_vers_nord)
intro.ajouter_choix("Aller vers le sud", aller_vers_sud)
aller_vers_nord.ajouter_choix("Explorer la cabane", explorer_cabane)

# Création du livre et lancement
livre = Livre(intro)
livre.lancer()
```

---

## Étape 5 : Ajouter des fonctionnalités avancées 

Pour ceux qui veulent aller plus loin, voici des idées de fonctionnalités bonus :
1. **Inventaire** : Ajoutez une classe `Personnage` pour gérer un inventaire, des points de vie, etc. (Revoir l'évaluation de POO...)
2. **Sauvegarde** : Implémentez un système de sauvegarde pour reprendre l’histoire au dernier chapitre. (Quelle structure ?)
3. **Génération aléatoire** : Ajoutez des éléments imprévisibles dans l’histoire grâce à des choix aléatoires (rappel : `random`).

---

## Étape 6 : Présentation des projets 

1. Travaillez en groupes pour créer vos propres livres interactifs.
2. Testez vos histoires entre vous pour identifier les bugs.
3. **Présentation finale** pour comparer les projets.

---

Auteur : Florian Mathieu

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.