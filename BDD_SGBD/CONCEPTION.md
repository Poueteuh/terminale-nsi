## Conception de Bases de Données : Modèle Entité-Association et Schéma Relationnel

### Bien modéliser, c'est tout un art

>  Créer une base de données sans réfléchir à sa conception, c’est comme construire une maison sans plan. Cela peut tenir un temps, mais dès qu’il y a des modifications à faire, tout s’écroule. Pour éviter cela, on utilise des outils et méthodes de conception qui permettent de structurer les données de manière cohérente et évolutive.

### Le Problème de la Redondance des Données

#### Qu’est-ce que la redondance ?

La redondance se produit lorsqu’une même information est stockée à plusieurs endroits. Cela peut entraîner des incohérences et alourdir inutilement la base.

#### **Exemple** : Table sans normalisation

| ID_Vente | Nom_Client | Email            | Produit |
| -------- | ---------- | ---------------- | ------- |
| 1        | Dupont     | dupont@gmail.com | Livre   |
| 2        | Dupont     | dupont@gmail.com | Stylo   |
| 3        | Dupont     | dupont@gmail.com | Cahier  |

Le client **Dupont** est répété trois fois. Imaginons qu’il change d’adresse email, il faudra le modifier partout, au risque d’oublier une ligne.

#### Pourquoi éviter la redondance ?

- **Risque d'incohérences** : Un email modifié sur une seule ligne entraîne des données erronées.
- **Espace perdu** : Si une base contient des millions d’enregistrements, ces duplications gaspillent de l’espace.
- **Maintenance compliquée** : Plus il y a de redondance, plus la gestion devient laborieuse.

------

### Modèle Entité-Association (MCD)

#### Définition

Le **Modèle Conceptuel de Données (MCD)**, ou **Modèle Entité-Association**, permet de représenter de manière visuelle les données d'un système, leurs relations, et leurs propriétés.

- **Entités** : Ce sont les objets ou concepts du système. Par exemple, un `Client`, un `Produit`.
- **Associations** : Ce sont les relations entre les entités. Par exemple, un `Client` *achète* un `Produit`.
- **Attributs** : Ce sont les propriétés des entités ou des associations. Par exemple, `Nom_Client`, `Email`.

#### Exemple simple de MCD

Imaginons une boutique où des clients achètent des produits.

[Client] — achète —< [Vente] >— concerne —> [Produit]



- L'entité **Client** possède des attributs comme `Nom` et `Email`.
- L'entité **Produit** possède des attributs comme `Nom_Produit` et `Prix`.
- L'entité **Vente** représente l'achat d'un produit par un client, avec des attributs comme `Date`.

#### Pourquoi faire un MCD ?

Le MCD permet de poser une base claire avant d’implémenter. C’est un peu comme un plan d’architecte : mieux vaut tout prévoir avant de commencer à coder.

-----

### Du MCD au Schéma Relationnel

### Conversion des Entités
Chaque **entité** du MCD devient une **table** dans le schéma relationnel. Les **attributs** des entités deviennent les **colonnes** des tables.

Exemple :  
Entité **Client** → Table **Client(ID_Client, Nom_Client, Email)**

#### Conversion des Associations

Les **associations** peuvent :
- Devenir une table séparée (notamment pour les associations n-n).
- Être représentées par une clé étrangère dans une table.

Exemple :  
Association `achète` (entre `Client` et `Produit`) devient une table **Vente(ID_Vente, ID_Client, ID_Produit, Date)**.

#### Exemple complet de schéma relationnel

À partir de notre MCD, voici le schéma relationnel correspondant :

- **Client**(ID_Client, Nom_Client, Email)
- **Produit**(ID_Produit, Nom_Produit, Prix)
- **Vente**(ID_Vente, Date, ID_Client, ID_Produit)

-----

### Contraintes d’Intégrité

#### Intégrité d'Entité

La **clé primaire** de chaque table doit être unique et non nulle. Elle garantit que chaque enregistrement est identifiable de manière unique.

Exemple :  
La clé primaire de la table **Client** est `ID_Client`. Deux clients ne peuvent pas avoir le même `ID_Client`.

#### Intégrité Référentielle

Les **clés étrangères** assurent que les relations entre les tables restent cohérentes. Une clé étrangère doit toujours pointer vers une clé primaire existante.

Exemple :  
Dans la table **Vente**, `ID_Client` est une clé étrangère qui doit correspondre à un `ID_Client` existant dans la table **Client**.

#### Intégrité de Domaine

Chaque attribut doit contenir des valeurs valides en fonction de son type (domaine). Par exemple, un champ `Prix` doit contenir un nombre positif.

------

### La Normalisation : Optimiser la Base

#### Introduction à la Normalisation

La normalisation est un processus pour structurer une base et réduire la redondance. Voici les trois premières **formes normales** (FN) :
1. **1NF** : Chaque colonne contient des valeurs atomiques.
2. **2NF** : Les colonnes dépendent entièrement de la clé primaire.
3. **3NF** : Pas de dépendance transitives entre attributs.

---

## Anecdote : Les erreurs d’un mauvais modèle

Dans les années 2000, un célèbre site de commerce a perdu des milliers d’euros à cause d’une mauvaise conception de sa base de données. Une erreur de relation entre les tables de **commande** et **client** a permis à des utilisateurs de modifier leurs commandes après validation… Certains ont payé 1€ pour un téléviseur ! Moralité : une base mal conçue peut coûter très cher.

-----

### Conclusion : Une base bien modélisée, c'est un jeu d'enfant

La modélisation est une étape clé dans la conception des bases de données. Avec un bon MCD, un schéma relationnel clair, et des contraintes bien définies, vous serez prêts à créer des bases robustes et cohérentes.

**Prochaines étapes** :
- Créer un MCD pour un projet simple (ex : gestion de bibliothèque, boutique en ligne).
- Passer du MCD au schéma relationnel.
- Implémenter le tout en SQL.

Et souvenez-vous : "Bien modéliser, c’est éviter les migraines de demain !"

-----------

#### Bibliographie

- Prépabac (Hatier)
- [Site de Gilles Lassus](https://glassus.github.io/terminale_nsi/)

-----------

Auteur : Florian Mathieu

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.