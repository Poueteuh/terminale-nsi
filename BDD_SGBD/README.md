## Conception des bases de données



--------

### Le programme



<img src="assets/bo_sgbd.png" alt="bo_sgb" style="zoom:67%;" />



## Introduction : Pourquoi les bases de données ?

> Imaginez que vous soyez responsable d'un magasin de jeux vidéo. Vous commencez par gérer vos stocks sur un simple fichier Excel : titre du jeu, console, stock disponible. Tout va bien, jusqu'à ce que les ventes explosent ! Vous vous retrouvez avec des centaines de jeux, des promotions à gérer, des clients à fidéliser… et là, votre fichier Excel montre ses limites : lenteur, erreurs de saisie, données dupliquées. **Bienvenue dans le monde des bases de données !**

Les bases de données permettent de structurer, organiser et interroger les données de manière efficace. Avant d'aller plus loin, faisons un petit détour historique.

### Anecdote historique : Edgar Codd et les bases relationnelles
Dans les années 1970, un certain **Edgar F. Codd**, mathématicien chez IBM, invente le modèle relationnel, qui repose sur des tables reliées entre elles par des relations logiques. Ce modèle, simple mais puissant, révolutionne la manière dont les données sont gérées. 

---

Avec le big data, il a fallu trouver une solution pour permettre aux entreprises de manipuler ces données entrantes. C'est alors que sont nés les premiers **S**ystème de **G**estion de **B**ases de **D**onnées, appelé **SGBD**.

Ces derniers ont plusieurs fonctions dont 4 qui sont primordiales :

- la sauvegarde 
- la mise en forme de données 
- l'interrogation 
- la recherche

Mais avant de voir comment ces SGBD fonctionnent, voyons les limites des autres systèmes de gestion de données.

## Les limites des structures plates

### Définition d'une structure plate
Une structure plate est un tableau bidimensionnel, comme une feuille Excel ou une table simple dans un fichier CSV. Chaque ligne représente un enregistrement, et chaque colonne, un attribut (ou champ). Exemple :

| ID   | Nom   | Console | Stock |
| ---- | ----- | ------- | ----- |
| 1    | Mario | Switch  | 50    |
| 2    | Zelda | Switch  | 30    |
| 3    | FIFA  | PS5     | 20    |

-----------------

### Limites de cette approche

#### Redondance des données
Supposons que vous ajoutiez une colonne pour l'éditeur de chaque jeu :

| ID   | Nom   | Console | Stock | Éditeur  |
| ---- | ----- | ------- | ----- | -------- |
| 1    | Mario | Switch  | 50    | Nintendo |
| 2    | Zelda | Switch  | 30    | Nintendo |
| 3    | FIFA  | PS5     | 20    | EA       |

Ici, "Nintendo" est répété plusieurs fois. En cas d'erreur de saisie (`Nintnedo`), les données deviennent incohérentes.

#### **1.2.2 Difficile à maintenir**
Si l'éditeur de "Zelda" change, il faut parcourir tout le tableau pour le modifier, ce qui est fastidieux et sujet aux erreurs.

#### **1.2.3 Limite des relations**
Il devient complexe de représenter les relations entre différentes entités. Par exemple, comment savoir quel client a acheté quel jeu ? Cela nécessiterait des tableaux imbriqués ou des fichiers séparés, augmentant le risque d'incohérences.

---

## Vocabulaire des schémas relationnels

### Table (ou relation)
Une table est un ensemble de lignes (ou enregistrements) et de colonnes (ou attributs). Chaque table représente une entité. Par exemple, une table `Jeux` pourrait ressembler à :

| ID   | Nom   | Console |
| ---- | ----- | ------- |
| 1    | Mario | Switch  |
| 2    | Zelda | Switch  |
| 3    | FIFA  | PS5     |

### Enregistrement (ou ligne, ou tuple)
Une ligne de la table représente une occurrence d'une entité. Exemple : `(1, Mario, Switch)`.

### Attribut (ou colonne)
Chaque colonne représente une propriété de l'entité. Exemple : `Nom`, `Console`.

### Clé primaire (Primary Key)
Un attribut (ou un ensemble d'attributs) qui permet d'identifier de manière unique chaque enregistrement. Exemple : l'attribut `ID` dans notre table `Jeux`.

### Clé étrangère (Foreign Key)
Un attribut qui établit une relation entre deux tables. Exemple : si nous avons une table `Ventes` indiquant quel client a acheté quel jeu, la clé étrangère pourrait être l'ID du jeu.

**Table Ventes :**

| ID_vente | ID_jeu | ID_client |
| -------- | ------ | --------- |
| 1        | 1      | 1001      |
| 2        | 2      | 1002      |

### Relation
Une relation représente le lien entre deux tables via des clés primaires et étrangères.

### Schéma relationnel
Un schéma relationnel décrit l'organisation des tables et les relations entre elles. Exemple d'un schéma simplifié :

- **Jeux**(ID, Nom, Console)  
- **Clients**(ID_client, Nom_client)  
- **Ventes**(ID_vente, ID_jeu, ID_client)  

-----------

### Mots clés

| Mot                         | Définition                                                   |
| --------------------------- | ------------------------------------------------------------ |
| **Attribut**                | Colonne d'une table, représentant une propriété ou caractéristique d'une entité. |
| **Domaine**                 | Ensemble des valeurs possibles pour un attribut donné.       |
| **Schéma**                  | Description formelle des structures d'une base de données (tables, relations, clés, etc.). |
| **Table / Relation**        | Structure contenant des données sous forme de lignes (enregistrements) et de colonnes (attributs). |
| **Élément (Tuple)**         | Ligne d’une table, représentant un enregistrement unique.    |
| **Cardinal**                | Nombre de lignes (tuples) dans une table.                    |
| **Clé primaire**            | Attribut ou combinaison d'attributs permettant d'identifier de manière unique chaque enregistrement d'une table. |
| **Clé étrangère**           | Attribut d’une table qui fait référence à une clé primaire d'une autre table, établissant une relation entre elles. |
| **Index**                   | Structure de données qui améliore les performances des recherches dans une table. |
| **Relation**                | Lien logique entre deux tables, souvent via des clés primaires et étrangères. |
| **Intégrité référentielle** | Contrainte qui garantit que les clés étrangères pointent vers des valeurs existantes dans les tables référencées. |
| **Intégrité d'entité**      | Contrainte qui garantit que la clé primaire est unique et non nulle pour chaque enregistrement. |
| **Jointure**                | Opération qui permet de combiner des données issues de plusieurs tables en utilisant des relations logiques définies entre elles. |

---

## Conclusion : De la théorie à la pratique

Nous avons vu que les structures plates, bien que simples, sont limitées en termes de redondance, de maintenance et de gestion des relations. Le modèle relationnel, basé sur des tables et des relations, permet de structurer efficacement les données tout en minimisant ces problèmes.

Dans les prochains cours, nous apprendrons à :
- Concevoir des schémas relationnels.
- Utiliser des outils comme SQL pour interagir avec des bases de données.
- Optimiser nos bases pour des performances et une cohérence maximales.

Et n'oubliez pas : "Bien modéliser, c'est déjà réussir à moitié !"

--------

Auteur : Florian Mathieu

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.