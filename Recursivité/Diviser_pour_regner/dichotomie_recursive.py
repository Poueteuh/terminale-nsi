def recherche(tab:list,x:int)-> bool:
    if len(tab) == 0:
        return False
    m = len(tab) // 2
    if tab[m] == x:
        return True
    elif tab [m] < x:
        return recherche(tab[m+1 :],x)
    else:
        return recherche(tab[:m],x)