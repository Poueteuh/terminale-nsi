# TD Récursivité :

------

## 1. Application du cours 

### 1. 1. Fonction somme :

```python
def somme(n) :
    if n == 0 :
        return 0
    else :
        return n + somme(n-1)

```

1) Combien d'appel de fonction sont nécessaire pour somme(5) ? somme(n) ? 

### 1. 2. Fonction factorielle  :

1. Ecrire le code de la fonction *factorielle*(n) étant défini comme : 

   $$
   \text{factorielle}(n) = 
   \begin{cases} 
       1 & \text{si } n = 0 \\
       n \times \text{factorielle}(n - 1) & \text{sinon.}
   \end{cases}
   $$
   

2) Quels seront les appels effectués pour obtenir factorielle(4) ?

## 2. TD 

### 2. 1. Fonction mystère :

1. Que fait la fonction mystère ci-dessous : 

   ```python
   def mystere(i,k):
       if i<=k :
       	print(i)
           mystère(i+1,k)
   ```

### 2. 2. Nombre de chiffre d'un nombre :

Ecrire une fonction nb_chiffre(n) permettant d'obtenir le nombre de chiffre d'un nombre :

```python
>>> nb_chiffre(9)
1
>>> nb_chiffre(99)
2
```

### 2. 3. Maximum d'un tableau :

Ecrire une fonction maximum(t) permettant d'obtenir le nombre le plus grand d'un tableau :

> ​	Conseils : 
>
> - Utilisez la fonction max(a,b)
> - Les slices 
>   - t = [0,1,2] 
>   - t[2:] => [2]

## 3. Bonus : 

### 3. 1. Suite de Syracuse :

La suite de syracuse est une suite définie comme : 

$$
U_{n+1} = 
\begin{cases} 
    U_n / 2 & \text{si } U_n \text{ est pair} \\
    3U_n + 1 & \text{sinon}
\end{cases}
$$
Sachant que U<sub>0</sub> est supérieur à 1

1. Ecrire la fonction *syracuse(u)* affichant les valeurs de la suite de syracuse. 
   La suite s'arrête lorsque *u* est inférieur ou égal à 1 

