class Pile :
    
    def __init__(self) :
        """
        Méthode constructeur
        """
        self.pile = []
        
    def empile(self,x) :
        """
        Méthode qui enpile un élément
        param x : () Elément x à enpiler
        """
        self.pile.append(x)

    def depile(self) :
        """
        Méthode qui defile un élément de la pile
        return : (bool/ ) False si impossible de retirer l'élément / Elément sinon
        """
        if self.est_vide() :
            return False
        else :
            return self.pile.pop()
            
        
    def est_vide(self):
        """Méthode calculant la longueur de la pile"""
        return self.pile == []
    
    def top(self):
        """Méthode renvoyant l'élément au dessus de la pile"""
        return self.pile[-1]
    
    def __repr__(self):
        long = len(self.pile)-1
        s = ''
        while long >= 0 :
            s+=str(self.pile[long])+'\n'
            long-=1
        return s
    
    def taille(self):
        """Méthode permettant de connaître la taille de la pile"""
        return len(self.pile)
    
    def sort_stack(self):
        """Méthode qui renvoie la pile trié"""
        pile_tmp = Pile()
        while not self.est_vide() :
            val_tmp = self.depile()
            while pile_tmp.taille() != 0 and pile_tmp.top() < val_tmp:
                self.empile(pile_tmp.depile())
            pile_tmp.empile(val_tmp)
        p.pile = pile_tmp.pile
        return p


p = Pile()
p.empile(7)
p.empile(1)
p.empile(3)
p.empile(9)
    