# **TP**

## **Première fonction : lancer de dé**

Question 0 - Créer un module ```de.py```.

Question 1 - Définir une fonction ```de``` qui simule un lancer de dé à 6 faces. On rappel que la fonction *randint* du module **random** permet le renvoie d'une valeur pseudo-aléatoire.

```python
>>> de() in [1, 2, 3, 4, 5, 6]
True
```

Question 2 - Modifier la fonction ```de``` en lui donnant un nouveau paramètre *n* permettant de simuler un lancer à *n* faces.

```python
>>> de(12) in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
True
```

Question 3 - Ajouter la docstring **et** la doctest de la fonction ```de```.

## **Année bissextile**

Question 4 - Créer un nouveau module ```bissextile.py```.

Question 5 - Définir une fonction qui demande à l'utilisateur de saisir une année. On donnera uniquement la docstring de cette fonction.

**Attention ! ```input``` renvoie une __chaine de caractère__ !**

Question 6 - Définir une fonction qui prend une année en paramètre et qui renvoie Vrai si l'année est bissextile, Faux sinon. On donnera la docstring **et** la doctest de cette fonction.

```python
>>> verifie(2000)
True
>>> verifie(2022)
False
>>> verifie(1900)
False
```

## **Fusion de module**

Question 7 - Créer un module ```fusion.py```.

Question 8 - Dans le module *fusion*, importer les modules *de* et *bissextile*. Utiliser les fonctions de ces modules pour définir la fonction *avance* qui a pour documentation :

```python
def avance(dep, pas, arr):
    """
    Avance d'année en année selon le pas en paramètre et en partant de l'année de départ jusqu'à l'arriver et renvoie le nombre d'année bissextile rencontrée.

    :param dep: (int) l'année de départ
    :param pas: (int) le pas
    :param arr: (int) l'année d'arrivée
    :return: (int) le nombre d'année bissextile rencontrée

    Exemple:
    >>> avance(2020, 2, 2024)
    2
    >>> avance(2018, 4, 2024)
    0
    """
```

## **Quelques documentations**

Dans un module ```arithmetique.py```, on retrouve 3 fonctions :

- **expo** qui prend en paramètres les entiers *a*, *b* et *n* et calcule le reste de la division par *n* de *a* puissance *b* ;
- **pgcd** qui prend en paramètres les entiers *a* et *b* et renvoie le plus grand diviseur commun entre *a* et *b* ;
- **decomposition** qui prend un entier positif *n* en paramètre et renvoie la liste de ses facteurs premiers ainsi que leur multiplicité.

Question 9 - Proposer des docstrings pour ces trois fonctions, il n'est pas demandé de donner le code des fonctions.

Question 10 - Ajouter la doctest de ces fonctions.



-------

Auteurs : Florian Mathieu, Enzo Frémeaux, Thimothée Decooster

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.