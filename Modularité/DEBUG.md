## Débug

> Savoir débugger, c'est savoir sauver son code.
> Comprendre pourquoi il ne fonctionne pas, traquer, chercher la petite erreur, c'est un pan entier du métier de développeur

## Introduction

Après avoir étudié la modularité, il est crucial de comprendre comment détecter, analyser et corriger les erreurs dans notre code. 

Pour cela, nous allons utiliser le débugging. Il va s'agir de revenir sur nos traces, de vérifier chaque ligne de code.
Et dans ce but, nous pourrons bénéficier de l'aide de l'interpréteur Python : celui-ci va nous fournir un message d'erreur qu'on appellera ***exception***.

Les erreurs, également appelées bugs, peuvent se produire à différents niveaux, y compris les erreurs de syntaxe, les erreurs d'exécution et les erreurs logiques.



### Types d'erreurs en Python

### Erreurs de syntaxe

Ce sont des erreurs dans la structure du code, telles que des parenthèses manquantes ou une mauvaise indentation.

Exemple :

```python
 print("Hello, world"
```

### Erreurs d'exécution

Ces erreurs se produisent lorsque le programme est en cours d'exécution et provoquent l'arrêt du programme.

Exemple :

  ```python
    result = 10 / 0
    print (result)
  ```

### Erreurs logiques

Ces erreurs ne provoquent pas l'arrêt du programme, mais produisent des résultats incorrects.

Exemple :

  ```python
  def somme(a, b):
  
  ​    return a - b # L'erreur est ici, il faut utiliser + au lieu de -
  ```

-------

## Techniques de Débugging

### Lecture attentive du message d'erreur

Python fournit des messages d'erreur détaillés qui incluent le type d'erreur et l'emplacement où elle s'est produite. Lisez attentivement ces messages pour comprendre la nature du bug.



### Utilisation de `print` pour le débugging

Ajoutez des instructions `print` pour afficher la valeur des variables à différentes étapes de l'exécution du programme.

Exemple :

  ```python
   def somme(a, b):
  
  ​    print(f"a = {a}, b = {b}")
  
  ​    return a + b
  ```



### Utilisation de l'instruction `assert`



Utilisez `assert` pour vérifier que certaines conditions sont vraies à différents points de votre programme.

Exemple :

  ```python
  def somme(a, b):
  
  ​    assert isinstance(a, int), "a doit être un entier"
  
  ​    assert isinstance(b, int), "b doit être un entier"
  
  ​    return a + b
  ```



### Utilisation de l'environnement de débugging intégré (IDE)



Les IDE comme Thonny ou VSCode offrent des outils de débugging intégrés qui permettent de définir des points d'arrêt, d'exécuter le code pas à pas et d'inspecter les valeurs des variables.



### Exemple Pratique de Débugging

Prenons un exemple de programme avec une erreur logique et voyons comment utiliser les techniques de débugging pour corriger cette erreur.

Programme avec une erreur :

```python
def moyenne(liste):

  somme = 0

  for valeur in liste:

​    somme += valeur

  return somme / len(liste)

notes = [15, 18, 12, 9]

print("La moyenne des notes est :", moyenne(notes))


```

--------

### Anecdote historique

Le saviez vous ? Contrairement aux croyances, ce n'est pas Grace Hopper qui a inventé le terme ***bug***, car celui ci était déjà utilisé pour décrire des problèmes de radar durant la seconde guerre mondiale.

Néanmoins, la postérité aura attribué ce mot à la developpeuse, qui aurait trouvé un insecte dans l'ordinateur de type Mark II.

------

Auteur : Florian Mathieu, Enzo Frémeaux, Thimothée Decooster.

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.