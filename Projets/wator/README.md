#  TP Python 

### Le Wator 

Durée : Le temps pour les requins de manger tous les thons ?



## Principe

Il s'agit d'un TP qui vous permettra de générer votre propre simulation "proie - prédateur".

--------------------
## Contexte

- TP à faire en solo ou en binôme, une machine par binôme.
- 2 Séances de 2H pour en arriver au bout.
- Support en markdown avec du python intégré pour fournir aux élèves certaines fonctions et d'afficher le résultat / courbe.


----------------

## Table des matières

1. [Introduction](#Introduction)

2. [Fonctionnement](###Fonctionnement)

3. [Algorithme](###Algorithme)	

4. [Programmation](#Programmation)

   


 ###  Introduction et règles de jeu

Le Wator se présente comme un jeu proie - prédateur, sur une grille qui comportera trois types de cases :

* Des cases vides
* Des cases contenant des prédateurs (Requins)
* Des cases contenant des proies (Thons)
  

### **Les poissons**
Les prédateurs mangent dès qu'ils entrent sur une case contenant une proie, qui, elle, disparait donc.

Pour survivre, un requin doit manger un thon de manière régulière. En revanche, le thon ne disparait que s'il est mangé.

Les deux espèces sont caractérisées par des variables qui sont communes à tous les individus de l'espèce en question :

Les thons ont un temps de gestation : On utilisera une variable appelée *durée de gestation des thons* pour la représenter.

Chez les requins, le temps de gestation est également présent mais de plus, ici, on devra gérer une energie. Ces deux valeurs seront initialisées à une valeur commune à tous les requins, appelées respectivement *durée de gestation des requins* et *énergie des requins*.

-----------------------

### Fonctionnement

Lors de chaque étape de la simulation, nous allons répéter plusieurs étapes :

Pour les **_thons_** :

* Le thon va essayer de se déplacer sur une case vide adjacente.
  * S'il en existe une, le thon se déplace vers cette
    case.
  * Il reste sur place sinon.

* La variable *durée de gestation des thons* voit sa valeur réduite de 1.
* Si ce temps arrive à 0, le thon donne naissance à un nouveau
  thon qui nait sur la case qu'il vient de quitter **_uniquement si celui ci s'est déplacé_**
   *  Le temps de gestation est alors remis à sa valeur initiale.
  


Pour les **_requins_** :

* Le requin perd 1 point d'_energie_
  * Si le niveau d'énergie du requin est à 0, il meurt. 
  
* Le requin choisit aléatoirement parmi ses voisines
une case occupée par un thon :

    * S'il en existe une, le requin se
déplace vers cette case et mange le thon. Son niveau d'_énergie_
est alors remis à sa valeur initiale. 

    * Sinon il cherche à se déplacer vers une case voisine vide choisie au hasard. Il reste sur place s'il n'y en a aucune.

* Le _temps de gestation du requin_ est diminué de 1 :
  
  *  Si ce temps arrive à 0, il donne naissance à un nouveau
    requin sur la case qu'il vient de quitter **si et uniquement s'il s'est déplacé**.

    * Son temps de gestation est remis à sa valeur initiale.

----------

**_A votre avis : si les paramètres sont mal ajustés que risque t-il de se produire_** ?

C'est exact : Le nombre de thons sera trop faible au bout d'un moment, et le cycle recherché disparaitra.

Pour notre simulation, il est necessaire de bien paramétrer notre programme afin de voir emerger un cycle qui va régir la population:

  * Lorsqu'il y aura suffisament de thons, le nombre de requin va accroitre.
  * Cela va donc réduire le nombre de thons...
  * Puis lorsqu'il y n'y aura plus assez de thons, les requins vont disparaitre.
  * Et de nouveaux, le nombre de thon va grandir...

La chose à éviter est donc l'extinction totale des thons, car on ne pourrait plus relancer la simulation.

Il faut donc respecter une règle :

```
temps gestation des thons < énergie des requins < durée gestation des requins

```

_Que peut-on choisir comme valeurs, selon vous?_

**Nous prendront ces valeurs ci :** 
  * Temps de gestation des thons : 2 
  * Energie des requins : 3 
  * Durée de gestations des requins : 5


Question :

Il y a un paramètre important qu'il nous reste à gérer :
**Combien de thons et de requins doit on initialiser au lancement de notre simulation ?**



### Algorithme


Avant de commencer à travailler sur le code, il est necessaire de revenir sur l'algorithme. Celui ci nous permettra d'avoir une feuille de route claire, et de savoir exactement ce que l'on doit faire. Ne restera plus qu'à réaliser cela à travers python.


Ici, nous avons un exemple d'algorithme sur la gestion des thons:

![Algorithme_thon](assets/algo_thon.png)

Trouvez vous cet algorithme fonctionnel ?

**_Ecrire un algorithme similaire pour les requins._**

L'important ici, est de bien couvrir tous les cas de figure possibles :

Plus nous seront précis dans l'algorithme, plus le travail de programmation sera facile.



------------

### Programmation

Dans le but de faciliter notre tâche, voilà la marche à suivre :

* Créer une classe Animal, qui comprendra les différents paramètres qui vont régir l'évolution d'un thon ou d'un requin par exemple
* Créer une classe World, dans laquelle nous pourrons définir toutes les fonctions necessaires au bon fonctionnement de notre simulation
* Appeler les fonctions pour lancer la simulation
  
  C'est parti !

  Pour commencer, appelons donc les bibliothèques que nous pourrions avoir besoin :

Question : trouvez le mot clé permettant d'importer les bibliothèques ci dessous, et ajoutez le à votre code.

```python
i***** matplotlib
       pylab
       random
       numpy
       time
```


Maintenant, pensons aux quelques variables que l'on aura besoin d'utiliser :

Pour créer la mer, et la diviser en grille, il nous faudra donc un nombre de lignes, et un nombre de colonnes :

```python

n_lignes = 25
n_colonnes = 25


```
Puis, on décomptera le nombre de thons et de requins afin de garder le ratio souhaité, puis nous initialiserons les paramètres que l'on appliquera:

```python
n_thon = 0
n_requin = 0

energie_init_thon = 10
energie_init_requin = 3
temps_gestation_thon = 2
temps_gestation_requin = 5


```

On peut donc passer aux choses sérieuses :

Créez une classe Animal, qui intégrera les paramètres associés aux thons et requins :

```python

class Animal():
    
    def __init__(self, id, energie_base, temps_gestation, x, y):
        ...

```

​        

Puis la classe World, dans laquelle on initialisera la grille que l'on utilise pour simuler une mer :

```python

class Wator():

    def __init__(self,...):

        self. = 
        self. = 
        self.grille = 
        self.nbcase = hauteur * largeur
        self.animaux = []
```

Deux choses importantes : 

Pour notre grille, il est primordial de choisir une structure efficace.

**Indice** : Comment crée t-on un tableau ou _array_ en python ?



Il faut ensuite ajouter les fonctions pour que notre océan se remplisse :

* Gérer le peuplement de la mer 
* Gérer le placement des animaux
* Une fonction pour créer la grille
* ???

Nous allons utiliser une structure pour retenir le type d'animal présent sur une case de coordonées x, y.

C'est ici que l'on peut reflechir au quota que l'on aura :

on propose 30% de thon, 10% de requins, et 60% de cases vides.

Vu le nombre d'animaux qu'il va falloir gérer, quelle structure proposez vous ?


```python
 def ...:
    = ...(a_id, energie_init[a_id], temps_gestation[a_id],  x, y)
    self.animaux.append(animal)
    self.grille[y][x] = animal

```
Ici, on applique donc notre quota :
```python

    def peupler(self, n_thon = 30, n_requin = 10 ):
        
        self.n_thon = n_thon
        self.n_requin = n_requin
```



Pour le placement des thons et requins, il va nous falloir utiliser une boucle, afin de générer des créatures jusqu'à ce que l'on ai le nombre de thons et de requins souhaité.

```python

    def generer_placement(n_animaux, a_id):

        ... i in ...(n_animaux):
            ... True:
                x, y = divmod(random.randrange(self.nbcase), self.hauteur)
                if not self.grid[y][x]:
                    self.pop_animal(a_id, x, y)
                    break       
    generer_placement(n_thon, thon)
    generer_placement(n_requin, requin)
    

```
```python

    
    def tableau_grille(self):
        return [[self.grille[y][x].id if self.grille[y][x] else 0
                    for x in range(self.largeur)] for y in range(self.hauteur)]

```


Il est important de vérifier les cases voisines quand on fait déplacer un thon ou un requin :
D'abord, on cherche à obtenir leur coordonées.
Pour cela, on a besoin de p-upplets ou d'un dictionnaire : 

```python


        
    def voisin(self, x, y):
        
        voisin = {}
        for dx, dy in ((0,-1), (1,0), (0,1), (-1,0)):
            xp, yp = (x+dx) % self.largeur, (y+dy) % self.hauteur
            voisin[xp,yp] = self.grille[yp][xp]
        return voisin


```

Il ne nous reste plus qu'à créer la fonction qui permettra à notre mer, à notre monde, d'évoluer
```python

    def evolution(self, animal):
        voisin = self.voisin(animal.x, animal.y)
        # Ajouter +1 à la gestation.
        déplacement = False
        Si l'animal est un requin
            try:
                xp, yp = utiliser la bibliothèque random...
                ..............
                ...................
        
        if not déplacement:
            try:
                xp, yp = random.choice([position
                            for position in voisin if voisin[position]==case])
                if animal.id != thon:
                    animal.energie -= 1
                déplacement = True
            except IndexError:
              
                xp, yp = animal.x, animal.y
                
                
        if animal.energie < 0:
            
            animal.vie = False
            self.grille[animal.y][animal.x] = case
        elif déplacement:
            x, y = animal.x, animal.y
            animal.x, animal.y = xp, yp
            self.grille[yp][xp] = animal
            if animal.gestation >= animal.temps_gestation:
                animal.gestation = 0
                self.pop_animal(animal.id, x, y)
            else:
                self.grille[y][x] = case
                



    def progression(self):
        random.shuffle(self.animal)
        
        n_animal = len(self.animal)
        for i in range(n_animal):
            animal = self.animal[i]
            if not animal.vie:
                continue
            self.evolution(animal)
            
        self.animaux = [animal for animal in self.animaux if animaux.vie]
        
```

Mais après tout cela : comment voir ce que l'on a crée ?
Comment s'assurer que cela fonctionne ?
Il nous faut une représentation graphique :

```python



def image_grille(self):
        
        img = self.tableau_grille()
        trace = plt.figure(figsize=(10, 5), dpi = 36)
        axe = fig.add_subplot(100)
        return trace
    
    def afficher_image(self):
        trace = self.image_grill()
        plt.show()

```




​        
​    



